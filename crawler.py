#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import logging
from lxml import html
import requests
import ipaddress

def define_args():
    parser = argparse.ArgumentParser(description='Website scraper.')
    parser.add_argument('url', metavar='url', type=str,
                        help='Enter starting url for a crawler.')

    return parser

def validate_args(parser):
    args = parser.parse_args()

    return args


def get_response(url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 '
                      'Safari/537.36'
    }

    response = requests.get(url, headers=headers)

    if response.status_code != 200:
        print("Captcha found on %s" % url)

    source_code = response.content  # get string of source code from response
    html_body = html.document_fromstring(source_code)  # make HTML element object

    return html_body


def parse_proxy_list(html):

    proxy_list = []
    PROXY_TABLE = html.xpath('//tr//td//text()')

    for col in PROXY_TABLE:
        try:
            ipaddress.ip_address(col)
            proxy_list.append(col)
        except ValueError:
            continue

    return proxy_list


if __name__ == '__main__':
    # logging
    logging.basicConfig(format='%(asctime)s %(message)s', filename='log.log', level=logging.DEBUG)

    # define args stuff
    parser = define_args()
    valid_args = validate_args(parser)

    # Lets send a request to url
    html = get_response(valid_args.url)

    # Get proxy list
    proxy_list = parse_proxy_list(html)
